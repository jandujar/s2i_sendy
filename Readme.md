# Environment vars
 	$dbHost = getenv('MYSQL_HOSTNAME'); //MySQL Hostname
	$dbUser = getenv('MYSQL_USERNAME'); //MySQL Username
	$dbPass = getenv('MYSQL_ROOT_PASSWORD'); //MySQL Password
	$dbName = getenv('MYSQL_DATABASE'); //MySQL Database Name
	$dbPort = getenv('MYSQL_PORT'); //3306;	